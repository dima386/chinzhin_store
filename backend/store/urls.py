from django.urls import include, path
from . import views
from django.conf.urls.static import static

urlpatterns = [
    path('order/', views.order, name='order'),
    path('order/empty', views.order_empty, name='send_mail'),
    path('', views.index, name='home'),
    path('api/items/', views.items, name='items'),
    path('api/send_mail/', views.get_mail_info, name='send_mail'),

] 