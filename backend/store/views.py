from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.core.mail import send_mail
from django.conf import settings
import random
import json

item_list = [
    {'name': "images/items/0.png", 'id': 10000,
     'description': 'Увлажнитель воздуха STARWIND SHC1322, 3л, белый', 'price': 1650},
    {'name': "images/items/1.png", 'id': 10001,
     'description': 'Триммер PHILIPS HC3521/15 серебристый/черный', 'price': 2290},
    {'name': "images/items/2.png", 'id': 10002,
     'description': 'Фитнес-трекер HONOR Band 5 CRS-B19S, 0.95", розовый', 'price': 2390},
    {'name': "images/items/3.png", 'id': 10003,
     'description': 'Мышь A4TECH Bloody V3, игровая, оптическая, проводная, USB, черный', 'price': 960},
    {'name': "images/items/4.png", 'id': 10004,
     'description': 'Фитнес-трекер HONOR Band 5 CRS-B19S, 0.95", черный', 'price': 2390},
    {'name': "images/items/5.png", 'id': 10005,
     'description': 'Пылесос SINBO SVC 3497, 2500Вт, синий/серый', 'price': 5670},
    {'name': "images/items/6.png", 'id': 10006,
     'description': 'Планшет DIGMA Optima 7 Z800 Android 10.0 серебристый', 'price': 9490},
    {'name': "images/items/7.png", 'id': 10007,
     'description': 'Монитор игровой ACER Nitro RG241YPbiipx 23.8" черный', 'price': 16800},
    {'name': "images/items/8.png", 'id': 10008,
     'description': 'Экшн-камера DIGMA DiCam 310 4K, WiFi, черный', 'price': 2290},
    {'name': "images/items/9.png", 'id': 10009,
     'description': 'Умная колонка ЯНДЕКС c голосовым помощником Алисой, серебристый', 'price': 5670},
    {'name': "images/items/10.png", 'id': 10010,
     'description': 'Квадрокоптер DJI Mini 2 MT2PD Fly More Combo с камерой, серый', 'price': 60990},
    {'name': "images/items/11.png", 'id': 10011,
     'description': 'Шлем виртуальной реальности HTC Vive PRO Eye EEA, черный/синий', 'price': 11960},
    {'name': "images/items/12.png", 'id': 10012,
     'description': 'МФУ лазерный CANON i-Sensys MF445dw, A4, лазерный, черный', 'price': 35310},
    {'name': "images/items/13.png", 'id': 10013,
     'description': 'Смарт-часы AMAZFIT Bip U, 1.43", зеленый / зеленый', 'price': 4490},
    {'name': "images/items/14.png", 'id': 10014,
     'description': 'Кофемашина PHILIPS EP1224/00, серый/черный', 'price': 29990},
    {'name': "images/items/15.png", 'id': 10015,
     'description': 'Гироскутер MIZAR MZ10,5CN, 10.5", карбон', 'price': 12990},
]


def index(request):
    template = 'store/index.html'
    context = {'test': 'test'}
    return render(request, template, context)

def order_empty(request):
    return HttpResponseRedirect('/')


def order(request):
    template = 'store/basket.html'
    context = {'test': 'test'}
    return render(request, template, context)


def items(request):
    if request.method == "POST":
        order_list = []

        data = json.loads(request.body.decode())
        if data:
            data = [int(i) for i in data]
            for item in item_list:
                if item.get('id') in data:
                    order_list.append(item)

        return JsonResponse({'items': order_list})
    if request.method == "GET":
        return JsonResponse({'items': item_list})



def get_mail_info(request):
    if request.body.decode():
        data = json.loads(request.body.decode())
        data['order_num'] = random.randint(100000, 999999)
        Sender.send(data)
    else:
        return HttpResponseRedirect('')
    return JsonResponse({'data': data})


class Sender:
    def send(data):
        name = data.get('name')
        order_num = data.get('order_num')
        phone = data.get('phone')
        mail_to = [data.get('email')]
        subject = f'Тестовое задание, заказ №{order_num}'
        mail_from = settings.EMAIL_HOST_USER
        message = f'''{name}, заказ №{order_num} сформирован. В ближайшее время наш специалист свяжется с вами по телефону {phone}.'''

        send_mail(subject, message, mail_from, mail_to,
                  fail_silently=False)
        
        message = f'''Новый заказ от клиента {name} заказ номер: {order_num}, Телефон: {phone}, Почта для связи: {mail_to}'''
        send_mail('новый заказ', message, mail_from, ["chinzhin@mail.ru"],
                  fail_silently=False)


        
