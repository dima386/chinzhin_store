if (window.location.pathname === '/') {
    sendRequest('GET', "/api/items/")
        .then(
            data => downloadItems(data))

} else if (window.location.pathname === '/order/') {
    body = localStorage.getItem('items')
    sendRequest('POST', "/api/items/", JSON.parse(body))
        .then(
            data => downloadItemsOrder(data))
};


function sendRequest(method, url, body = null) {

    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest()

        xhr.open(method, url);
        xhr.responseType = 'json';
        xhr.onload = () => {
            if (xhr.status >= 400) {
                reject(xhr.response)
            } else {
                resolve(xhr.response)
            }
        }
        xhr.send(JSON.stringify(body));
    })
};


function downloadItems(data) {
    let container = document.getElementsByClassName("container");

    for (let i = 0; i < data.items.length; i++) {
        let item = document.createElement('div');
        item.classList.add("item");

        let imageName = data.items[i].name
        let description = data.items[i].description
        let price = data.items[i].price
        let id = data.items[i].id

        item.innerHTML = `
        <div class="img-block">
            <img class=img src="/static/${imageName}" alt="">
        </div>
        <div class="item-info">
            <div class="item-desc">
                <p> ${description} </p>
            </div>
            <div class="item-price">
                ${price} ₽
            </div>
            <button id=${id} onclick=addItemToBasket(this) class="add_basket">
                ДОБАВИТЬ В КОРЗИНУ
            </button>
        </div>`

        container[0].appendChild(item);
    }
    basket.checkBacket();
    basket.selectInBasketGreen();
};


function downloadItemsOrder(data) {
    let container = document.getElementsByClassName("container");

    for (let i = 0; i < data.items.length; i++) {
        let id = data.items[i].id
        let price = data.items[i].price
        let imageName = data.items[i].name
        let description = data.items[i].description

        let item = document.createElement('div');
        item.classList.add("item");
        item.id = id

        item.innerHTML = `
            <div class="img-block">
                <img class=img src="/static/${imageName}" alt="">
            </div>
            <div class="item-desc">  ${description} </div>
                <div class="reverser">
                    <div class="item-counter">
                        <div id="minus" class="plus-minus" onclick=BasketItems.countDown(this)>
                            <div class="minus" ></div>
                            </div>
                        <div id="counter-view" class="counter-view">
                            1
                        </div>
                            <div id="plus" class="plus-minus" onclick=BasketItems.countUp(this)>
                                <div class="plus" ></div>
                                </div>
                            </div>
                        <div class="item-price">
                                <h2>${price} ₽</h2>
                            </div>
                        </div>
                        <button onclick=BasketItems.remouve(this) class="close-button">
                            <div class="close-x-block">
                                <div class="close">
                                </div>
                            </div>
                            <div class="close-text">
                                Удалить
                            </div>
                            
                        </button>`

        container[0].appendChild(item);
    }
    BasketItems.recalculCount();
    BasketItems.recalcul();
    basket.checkBacket();
};


function addItemToBasket(identifier) {
    identifier.style.background = '#00A82D';

    if (identifier.innerHTML === 'В КОРЗИНЕ') {
        basket.goToBasket();
    } else {

        identifier.innerHTML = 'В КОРЗИНЕ';
        if (localStorage.items === undefined) {
            listId = [identifier.id];
            localStorage.items = JSON.stringify(listId);
        } else {
            let listIdStorage = localStorage.getItem("items");
            listId = JSON.parse(listIdStorage);
            listId.push(identifier.id);
            localStorage.items = JSON.stringify(listId);
        }
        basket.addItem();
        basket.checkBacket()
    }
};


const basket = {

    remouveItem() {
        let circle = document.getElementById('basket-count');
        circle.innerHTML = +circle.innerHTML - 1;
    },

    addItem() {
        let circle = document.getElementById('basket-count');
        circle.innerHTML = +circle.innerHTML + 1;
    },

    getCount() {
        let basket = document.getElementById('basket-count');
        return basket;
    },

    goToBasket() {
        let basket = document.getElementsByClassName("basket-text")[0];
        if (basket.classList.contains('basket__active')) {
            window.location.href = '/order/';
        }
    },

    checkBacket() {

        let inBasket = JSON.parse(localStorage.getItem('items'));
        let basket = document.getElementsByClassName("basket-text")[0];
        let circle = document.getElementsByClassName('circle');
        if (inBasket != null) {
            let count = inBasket.length;
            circle[0].innerHTML = count;
            if (Number(count) > 0) {
                circle[0].style.visibility = 'visible'
                basket.classList.add('basket__active');
            } else {
                circle[0].style.visibility = 'hidden';
                basket.classList.remove('basket__active');
            };

        };


    },

    selectInBasketGreen() {
        let inBasket = JSON.parse(localStorage.getItem('items'));
        if (inBasket == false) {
            return;
        }
        for (i in inBasket) {
            let identifier = document.getElementById(inBasket[i]);
            if (identifier) {
                identifier.style.background = '#00A82D';
                identifier.innerHTML = 'В КОРЗИНЕ';
            } else {
                localStorage.setItem('items', "[]");
            };

        };
    }
};


const SendOrder = {
    send() {
        let form = document.getElementById("submitform");
        let email = form.elements["email"].value;
        let name = form.elements["name"].value;
        let phone = form.elements["phone"].value;
        const body = {
            name: name,
            phone: phone,
            email: email,
        };
        sendRequest('POST', '/api/send_mail/', body)
            .then(
                data => this.showPopUp(data))
    },

    showPopUp(data) {

        let popup = document.getElementById("popup");
        let popup__content = document.getElementById("popup__content");
        let client__name = document.getElementById("client__name");
        let client__order = document.getElementById("client__order");
        let client__phone = document.getElementById("client__phone");
        client__name.innerHTML = `${data.data.name}, `;
        client__order.innerHTML = `№${data.data.order_num}`;
        client__phone.innerHTML = data.data.phone;

        popup.style.visibility = "visible";
        popup.style.opacity = 1;
        popup__content.style.transform = "translate(0px, 0px)";
        popup__content.style.opacity = 1;
        localStorage.removeItem('items');
    }
};


const BasketItems = {

    iget() {
        items = localStorage.getItem('items');
        return JSON.parse(items);
    },

    ipush(items) {
        items = JSON.stringify(items);
        localStorage.setItem('items', items);
    },

    remouve(button) {
        let item = button.parentElement;
        inOrder = this.iget()
        for (let i = 0; i < inOrder.length; i++) {
            if (inOrder[i] === item.id) {
                inOrder.splice(i, 1);
                this.ipush(inOrder);
                localStorage.removeItem(item.id);
            };
        };
        item.remove();
        basket.checkBacket();
        this.recalcul()
    },

    countUp(el) {
        let counterView = el.parentElement;
        let countEl = counterView.getElementsByClassName("counter-view");
        count = countEl[0].innerText;
        if (count < 10) {
            count = +count + 1
            countEl[0].innerText = count;
        };
        this.recalcul()
    },

    countDown(el) {
        let counterView = el.parentElement;
        let countEl = counterView.getElementsByClassName("counter-view");
        count = countEl[0].innerText;
        if (count > 1) {
            count = +count - 1
            countEl[0].innerText = count;
        };
        this.recalcul()
    },

    recalcul() {
        let allItems = document.getElementsByClassName('item');
        let allPrice;
        let count;
        let price;
        let itemSum = 0;
        let totalSum = 0;
        var item
        for (i = 0; i < allItems.length; i++) {
            item = allItems[i];
            allPrice = item.getElementsByClassName("reverser");
            count = allPrice[0].children[0].innerText;
            localStorage.setItem(item.id, count);
            price = allPrice[0].children[1].innerText;
            price = price.split(' ')[0];
            itemSum = (Number(count) * Number(price));
            totalSum = totalSum + itemSum;
        };
        totalPrice = document.getElementsByClassName("total-price")[0];
        totalPrice.textContent = `Сумма ${totalSum}\u00A0₽`;
        if (totalSum === 0) {
            window.location.href = '/order/empty';
        }


    },

    recalculCount() {
        let allItems = document.getElementsByClassName('item');
        let allPrice;
        let count;
        let item;
        for (i = 0; i < allItems.length; i++) {
            item = allItems[i];
            count = localStorage.getItem(item.id);
            if (count < 1) {
                count = 1
            };
            allPrice = item.getElementsByClassName("counter-view");
            allPrice[0].innerText = count;
        };



    },
};